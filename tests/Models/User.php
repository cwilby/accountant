<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Eventually\Relations\BelongsToMany;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Recordable, Authenticatable
{
    use \Altek\Accountant\Recordable;
    use \Altek\Eventually\Eventually;
    use \Illuminate\Auth\Authenticatable;

    /**
     * {@inheritdoc}
     */
    protected $table = 'users';

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_admin' => 'bool',
    ];

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'full_name',
    ];

    /**
     * Associated Articles.
     *
     * @return \Altek\Eventually\Relations\BelongsToMany
     */
    public function articles(): BelongsToMany
    {
        return $this->belongsToMany(Article::class)
            ->withPivot('liked')
            ->withTimestamps();
    }

    /**
     * Uppercase first name character accessor.
     *
     * @param string $value
     *
     * @return string
     */
    public function getFirstNameAttribute(string $value): string
    {
        return \ucfirst($value);
    }

    /**
     * Full name accessor.
     *
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return \sprintf('%s %s', $this->first_name, $this->last_name);
    }
}
